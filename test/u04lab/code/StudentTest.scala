package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

import Lists._
import Lists.List._


class StudentTest {

  var studentThomas: Student = Student("Thomas")
  val a: Course = Course("PPDS", "Gorini")
  val b: Course = Course("PPS", "Nardini")
  val c: Course = Course("PPS", "Nardini")
  val d: Course = Course("PCeD", "Nardini")
  var studentAldo: Student = Student("Aldo")

  studentThomas.enrolling(a, b)


  val cPPS: Course = Course("PPS","Viroli")
  val cPCD: Course = Course("PCD","Ricci")
  val cSDR: Course = Course("SDR","D'Angelo")
  val s1: Student = Student("mario",2015)
  val s2: Student = Student("gino",2016)
  val s3: Student = Student("rino") //defaults to 2017
  s1.enrolling(cPPS, cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS, cPCD, cSDR)




  @Test
  def testCourses(): Unit = {
    assertEquals(Cons("PCD", Cons("PPS", Nil())), s1.courses)
    assertEquals(Cons("PPS", Nil()), s2.courses)
    assertEquals(Cons("SDR", Cons("PCD", Cons("PPS", Nil()))), s3.courses)
  }

  @Test
  def testHasTeacher(): Unit = {
    assertTrue(studentThomas.hasTeacher("Gorini"))
    assertFalse(studentThomas hasTeacher "Gorini2")
    assertTrue(s1.hasTeacher("Ricci"))
  }

  @Test
  def testSameTeacher(): Unit = {

    assertEquals("Nardini", List(c, d) match {
      case sameTeacher(t) => t
      case _ => "Nothing"
    } )

    assertEquals("Nothing", List(a, b) match {
      case sameTeacher(t) => t
      case _ => "Nothing"
    } )

  }


}
