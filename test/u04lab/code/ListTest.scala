package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import Lists.List._

class ListTest {


  // EXERCISES:

  @Test
  def testFilterByFlatmap(): Unit ={
    assertEquals(Cons(20, Nil()), filterByFlatmap(Cons(10, Cons(20, Nil())))(_>15)) // Cons(20, Nil())
    assertEquals(Cons("a",Cons("bb", Nil())) ,filterByFlatmap(Cons("a", Cons("bb", Cons("ccc", Nil()))))( _.length <=2)) // Cons("a",Cons("bb", Nil()))
  }

  @Test
  def testAppenfByFold(): Unit ={
    assertEquals(Cons(3,Cons(7,Cons(1,Cons(5, Nil())))), appendByFold(Cons(3,Cons(7,Nil())), Cons(1,Cons(5,Nil())))) // Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  }

  @Test
  def testLength(): Unit ={
    assertEquals(0, length(Nil())) // 0
    assertEquals(4, length(Cons(3,Cons(7,Cons(1,Cons(5, Nil())))))) // 4
  }

}
