package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class ComplexTest {

  val re: Double = 1
  val im: Double = 1

  val a: Array[Complex] = Array(Complex(10,20), Complex(1,1), Complex(7,0))
  val c: Complex = a(0) + a(1) + a(2)
  val c2: Complex = a(0) * a(1)

  @Test
  def testComplexImplToString(): Unit ={
    assertNotEquals(new ComplexImpl(re,im).toString, new ComplexImpl(re, im).toString)
  }

  @Test
  def testComplexImplEquals(): Unit ={
    assertFalse(new ComplexImpl(re,im).equals(new ComplexImpl(re, im).toString))
  }

  @Test
  def testPlus(): Unit ={
    val res = a(0) + a(1) + a(2)
    assertEquals(18.0, res.re)
    assertEquals(21.0, res.im)
  }

  @Test
  def testMul(): Unit ={
    val res = a(0) * a(1)
    assertEquals(-10.0, res.re)
    assertEquals(30.0, res.im)
  }

  @Test
  def testCaseComplexImplToString(): Unit ={
    assertEquals(CaseComplexImpl(re,im).toString, CaseComplexImpl(re, im).toString)
  }

  @Test
  def testCaseComplexImplEquals(): Unit ={
    assertTrue(CaseComplexImpl(re,im).equals(CaseComplexImpl(re,im)))
  }

}
