package u04lab.code

import u04lab.code.Lists.List._
import u04lab.code.Lists._ // import custom List type (not the one in Scala stdlib)

trait Student {
  def name: String
  def year: Int

  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017): Student = new StudentImpl(name, year)
}

object Course {

  case class CourseImpl(name: String,
                        teacher: String) extends Course

  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)

}


object sameTeacher {
  def unapply(courses: List[Course]): Option[String] = {

    @scala.annotation.tailrec
    def checkTeacher(courseList: List[Course], teacher: Option[String]): Option[String] = courseList match {
      case Cons(head, tail) if teacher.isEmpty || teacher.get.equals(head.teacher) => checkTeacher(tail, Option(head.teacher))
      case Nil() if teacher.isDefined => Some(teacher.get)
      case _ => None
    }

    checkTeacher(courses, None)
  }
}


class StudentImpl(override val name: String,
                  override val year: Int = 2017) extends Student {

  private[this] var courseList: List[Course] = Nil()

  def enrolling(course: Course): Unit = contains(courseList)(_ equals course) match {
    case false => courseList = Cons(course, courseList)
    case _ => ()
  }

  override def courses: List[String] = map(this.courseList)(_.name)

  override def hasTeacher(teacher: String): Boolean = first(courseList)(_.teacher equals teacher) match {
    case Some(_) => true
    case None => false
  }

  override def enrolling(course: Course*): Unit = for (i <- course) enrolling(i)

  @scala.annotation.tailrec
  private def first[A](list: List[A])(pred: A=>Boolean): Option[A] = list match {
    case Cons(head, tail) if pred(head) => first(tail)(pred)
    case Nil() => Option.empty
  }
}


/** Hints:
 * - simply implement Course, e.g. with a case class
 * - implement Student with a StudentImpl keeping a private Set of courses
 * - try to implement in StudentImpl method courses with map
 * - try to implement in StudentImpl method hasTeacher with map and find
 * - check that the two println above work correctly
 * - refactor the code so that method enrolling accepts a variable argument Course*
 */
