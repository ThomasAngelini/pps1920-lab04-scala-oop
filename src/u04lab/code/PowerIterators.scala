package u04lab.code

import Optionals._
import Option._
import Lists._
import List._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A])
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = fromStream(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): Unit = fromStream(listToStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = fromStream(Stream(new Random().nextBoolean()))

  private def listToStream[A](list: List[A]): Stream[A] = {

    // This solution have better performance
/*    @scala.annotation.tailrec
    def _reverseListToStream(list: List[A], stream: Stream[A]): Stream[A] = list match {
      case Cons(head, tail) =>  _reverseListToStream(tail, head #:: stream)
      case Nil() => stream
    }

    _reverseListToStream(list, Stream()).reverse*/

    @scala.annotation.tailrec
    def _listToStream(list: List[A], stream: Stream[A]): Stream[A] = list match {
      case Cons(head, tail) =>  _listToStream(tail, stream :+ head)
      case Nil() => stream
    }
    _listToStream(list, Stream())
  }

  private def fromStream[A](stream: Stream[A]): PowerIterator[A] = {

    val iterator = stream.iterator

    new PowerIterator[A]() {

      var pastElement: List[A] = Nil()

      override def next(): Option[A] = {
        if (iterator.hasNext){
          val elem = iterator.next()
          pastElement = append(pastElement, Cons(elem, Nil()))
          Some(elem)
        }else{
          None()
        }
      }

      override def allSoFar(): List[A] = pastElement

      override def reversed(): PowerIterator[A] = fromStream(listToStream(reverse(pastElement)))
    }
  }
}
